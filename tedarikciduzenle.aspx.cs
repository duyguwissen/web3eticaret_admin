﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class tedarikciduzenle : System.Web.UI.Page
{
    Tedarikci t = new Tedarikci();
    il il = new il();
    ilce ilce = new ilce();
    VergiDairesi vd = new VergiDairesi();
    int vergiId;
    int index;
    protected void Page_Load(object sender, EventArgs e)
    {
        int tedarikId = Convert.ToInt32(Request.QueryString["tedarikciId"]);

        DataRow tedarikci = t.TedarikciGetir(tedarikId);

        if (!IsPostBack)
        {
            ddlIl.DataTextField = "il";
            ddlIl.DataValueField = "id";
            ddlIl.DataSource = il.IlListe();
            ddlIl.DataBind();

            int secilenIlId = Convert.ToInt32(ddlIl.SelectedValue.ToString());
            string secilenIl = ddlIl.SelectedItem.Text.ToString();
            Response.Write(secilenIl);

            ddlIlce.DataTextField = "ilce";//dropdownlistin text kısmına veritabanından gelen ilce kolonunu atıyor.
            ddlIlce.DataValueField = "id";
            ddlIlce.DataSource = ilce.IlceListe(secilenIlId);
            ddlIlce.DataBind();

            ddlVergi.DataTextField = "daire";
            ddlVergi.DataValueField = "id";
            ddlVergi.DataSource = vd.DaireListe(secilenIl);
            ddlVergi.DataBind();
            ddlVergi.SelectedIndex = index;

            txtTedarikciAd.Text = tedarikci["firmaAdi"].ToString();
            txtAdres.Text = tedarikci["adresDetay"].ToString();
            txtVergiNo.Text = tedarikci["vergiNo"].ToString();
            txtistel.Text = tedarikci["isTel"].ToString();
            txtceptel.Text = tedarikci["cepTel"].ToString();
            txtemail.Text = tedarikci["email"].ToString();
            ddlIl.SelectedValue = tedarikci["ilID"].ToString();
            ddlIlce.SelectedValue = tedarikci["ilceID"].ToString();
            ddlVergi.SelectedValue = tedarikci["vergiDairesiID"].ToString();
        }

        

        vergiId = Convert.ToInt32(tedarikci["vergiDairesiID"].ToString());
    }
    protected void ddlIl_SelectedIndexChanged(object sender, EventArgs e)
    {
        int secilenIlId = Convert.ToInt32(ddlIl.SelectedValue.ToString());
        string secilenIl = ddlIl.SelectedItem.Text.ToString();
        ddlIlce.DataSource = "";
        ddlIlce.DataTextField = "ilce";
        ddlIlce.DataValueField = "id";
        ddlIlce.DataSource = ilce.IlceListe(secilenIlId);
        ddlIlce.DataBind();

        ddlVergi.DataTextField = "daire";
        ddlVergi.DataValueField = "id";
        ddlVergi.DataSource = vd.DaireListe(secilenIl);
        ddlVergi.DataBind();
    }


    protected void ddlVergi_DataBound(object sender, EventArgs e)
    {
        ListItem item = ddlVergi.Items.FindByValue(vergiId.ToString());
        index = ddlIl.Items.IndexOf(item);
    }
}