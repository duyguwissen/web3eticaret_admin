﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="urundetay.aspx.cs" Inherits="urundetay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function() {
            $("#ozellikekle").on("click", function () {
                var tags = new Array();
                var say = 0;
                $("#tags").find("span").each(function () {
                    tags[say] = $(this).text();
                    say++;
                })
                alert(tags[0] + tags[1] + tags[2]);
            })
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="wrapper">
        <!-- Images gallery -->
        <div class="oneTwo">
            <div class="widget urun-ozellik">
                <div class="title"><h6>Ürün Özellikleri</h6></div>
                <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound1">
                    <ItemTemplate>
                        <div class="formRow">
                            <asp:Label ID="ozellik" runat="server" ToolTip='<%#Eval("ozellikId")%>'>
                                <strong><%#Eval("ozellikAdi") %>:</strong> &nbsp;
                            </asp:Label>
                                <asp:Repeater ID="Repeater2" runat="server">
                                    <ItemTemplate>
                                        <%#Eval("ozellikDetayi") %>
                                        <asp:LinkButton ID="LinkButton1" runat="server" class="tipS" original-title="Sil" CssClass='<%#Eval("ozellikDetayId") %>' OnClick="LinkButton1_Click">             
                                            <img src="images/icons/remove.png" alt="">
                                        </asp:LinkButton> &nbsp;
                                    </ItemTemplate>
                                </asp:Repeater>
                                
                                <div class="clear"></div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                 <div class="formRow">
                        <label for="tags">Detay</label>
                        <div class="formRight"><input type="text" id="tags" name="tags" class="tags" value="these,are,sample,tags" /></div>
                        <div class="clear"></div>
                    </div>
                <div class="formRow">
                    <div class="oneTwo">
                       
                        <div class="formRight">
                            <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList>
                        </div>               
                        
                    </div>
                    <div class="oneTwo" align="center">
                        <button id="ozellikekle" class="greenB">Ekle</button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="widget">
                <div class="title"><img src="images/icons/dark/images2.png" alt="" class="titleIcon"><h6>Ürün Resimleri</h6></div>
                <div class="gallery">
                   <ul>
                        <li><a href="images/big.png" title="" rel="lightbox"><img src="images/img.png" alt=""></a>
                            <div class="actions" style="display: none;">
                                <a href="#" title=""><img src="images/icons/update.png" alt=""></a>
                                <a href="#" title=""><img src="images/icons/delete.png" alt=""></a>
                            </div>
                        </li>
                       <li><a href="images/big.png" title="" rel="lightbox"><img src="images/img.png" alt=""></a>
                            <div class="actions" style="display: none;">
                                <a href="#" title=""><img src="images/icons/update.png" alt=""></a>
                                <a href="#" title=""><img src="images/icons/delete.png" alt=""></a>
                            </div>
                        </li>
                       <li><a href="images/big.png" title="" rel="lightbox"><img src="images/img.png" alt=""></a>
                            <div class="actions" style="display: none;">
                                <a href="#" title=""><img src="images/icons/update.png" alt=""></a>
                                <a href="#" title=""><img src="images/icons/delete.png" alt=""></a>
                            </div>
                        </li>
                       <li><a href="images/big.png" title="" rel="lightbox"><img src="images/img.png" alt=""></a>
                            <div class="actions" style="display: none;">
                                <a href="#" title=""><img src="images/icons/update.png" alt=""></a>
                                <a href="#" title=""><img src="images/icons/delete.png" alt=""></a>
                            </div>
                        </li>
                   </ul> 

                   <div class="fix"></div>
               </div>
            </div>
        </div>
        <div class="oneTwo">
            <div class="widget">
                <div class="title"><img src="images/icons/dark/list.png" alt="" class="titleIcon"><h6>Ürün Bilgileri</h6></div>
                <div class="formRow">
                    <div class="oneTwo" align="center">
                        <strong>Tarih:</strong> <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                         
                         <div class="clear"></div>
                    </div>
                    <div class="oneTwo"  align="center">
                        <strong>Ürün Kodu:</strong> <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                        <div class="clear"></div>
                   </div>
                </div>
                <div class="formRow">
                    <label>Ürün Adı:</label>
                    <div class="formRight"><asp:TextBox ID="urunAd" runat="server"></asp:TextBox></div>
                    <div class="clear"></div>
                </div>
               <div class="formRow">
                    <label>Kategori:</label>
                    <div class="formRight">
                         <asp:DropDownList ID="DropDownList1" runat="server">
                                
                         </asp:DropDownList> 
                    </div>                       
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="oneTwo  urun-select">
                        <label>Ücret: </label>
                        <div class="formRight">
                            <asp:TextBox ID="urunFiyat" runat="server"></asp:TextBox>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="oneTwo urun-select">
                        <label>Birim: </label>
                        <div class="formRight">
                            <asp:DropDownList ID="DropDownList2" runat="server">

                            </asp:DropDownList>
        
                        </div>               
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                    <div class="oneTwo  urun-select">
                        <label>Stok:</label>
                        <div class="formRight">
                            <asp:TextBox ID="urunStok" runat="server"></asp:TextBox>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="oneTwo urun-select">
                        <label>Cinsiyet: </label>
                        <div class="formRight">

                            <select name="select2" id="cinsiyet"  runat="server">
                                <option value="opt6">Erkek</option>
                                <option value="opt7">Kadın</option>
                            </select>           
                        </div>               
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                 <div class="formRow">
                    <label>Açıklama:</label>
                    <div class="formRight"><asp:TextBox ID="urunAciklama" Rows="8" runat="server" TextMode="MultiLine"></asp:TextBox></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow" align="right">
                    <asp:Button ID="Button1" runat="server" class="button dblueB" Text="Kaydet" OnClick="Button1_Click" />
                </div>

            </div>
        </div>
    </div>
</asp:Content>

