﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLogin_Click1(object sender, EventArgs e)
    {
        Uye kul = new Uye();
        kul.GirisYap(login.Value, pass.Value);

        if (kul.kullaniciAdi != null)
        {
            if (kul.yetki.Trim().ToLower()=="admin" || kul.yetki.Trim().ToLower()=="asistan")
            {

                Session["kontrol"] = kul;
                Response.Redirect("/default.aspx");
            }
            else {
                Response.Write("<script>alert('Yönetici paneline giriş izniniz yok.')</script>");
            }
        }

        else {
            Response.Write("<script>alert('Hatalı giriş yaptınız.')</script>");
        }
    }
}