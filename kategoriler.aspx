﻿<%@ Page Title="" Language="C#" MasterPageFile="Admin.master" AutoEventWireup="true" CodeFile="kategoriler.aspx.cs" Inherits="admin_kategoriler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <!--<script type="text/javascript">
            $(document).ready(function(){
                $("body").on("click", "#duzenlebtn", function () {
                    var labeldeger = $(this).parent().parent().find("label").html();
                    $(this).parent().parent().find("label").html("<input type='text' id='katadi' class='kategoriduzenle' value='"+labeldeger+"'/>");
                    $(this).parent().parent().find("span").html("<div class='formRight'><select id='cinsiyet'><option values='False'>Erkek</option><option value='True'>Kadın</option></select></div>");
                    $(this).parent().parent().find("label").css('text-align', 'center');
                })
            })
    </script>-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="wrapper">
        <div class="twoOne">
        <div class="widget">
            <div class="title">
                <img src="images/icons/dark/full2.png" alt="" class="titleIcon" /><h6>Kategoriler</h6>
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="display dTable">
                <thead>
                    <tr>
                        <th>Kategori Adı</th>
                        <th>Ürün Sayısı</th>
                        <th>Kategori Cinsiyet</th>
                        <th>İslem</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td><label><%#Eval("kategoriAd") %></label></td>
                                <td class="center"><%#Eval("UrunSayısı") %></td>
                                <td class="center"><span><%#Eval("kategoriCinsiyet") %></span></td>
                                <td class="actBtns">
                                    <a href="#" onclick="javascript:void(0)" class="tipS" original-title="Ürün Detayı" id="duzenlebtn" data-katid='<%#Eval("kategoriId") %>'>
                                        <img src="images/icons/edit.png" alt="">
                                    </a>
                                    &nbsp;<asp:LinkButton ID="LinkButton1" runat="server" class="tipS" original-title="Sil" ToolTip='<%#Eval("kategoriId") %>' OnClick="LinkButton1_Click">
                                        <img src="images/icons/remove.png" alt="">
                                    </asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                   <tr class="gradeX kategoriekle">
                                <td>
                                    <asp:TextBox ID="TextBox1" runat="server" placeholder="Kategori Adı"></asp:TextBox>
                                </td>
                                <td class="center">
                                    
                                </td>
                                <td class="center">
                                    <asp:DropDownList ID="DropDownList1" runat="server">
                                        <asp:ListItem Value="False">Erkek</asp:ListItem>
                                        <asp:ListItem Value="True">Kadın</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="actBtns">
                                    <asp:Button ID="Button1" class="button greenB" style="margin: 5px;" runat="server" Text="Button" OnClick="Button1_Click" />
                                   
                                </td>
                            </tr>
                </tbody>
            </table>
        </div>
        </div>
        <div class="oneThree">
            <div class="widget">
                <div class="title">
                    <img src="images/icons/dark/full2.png" alt="" class="titleIcon" /><h6>Kategoriler</h6>
                </div>
                <div class="formRow">
                    
                    <div class="formRight"><input type="text" value=""></div>
                    <div class="clear"></div>
                </div>
                <div class="formRow">
                   
                    <div class="formRight">
                    <asp:DropDownList ID="DropDownList3" runat="server">
                        <asp:ListItem Value="False">Erkek</asp:ListItem>
                        <asp:ListItem Value="True">Kadın</asp:ListItem>
                    </asp:DropDownList>
                        </div>
                    <div class="clear"></div>
                </div>
                <div class="formRow" align="center">
                                    <asp:Button ID="Button2" class="button greenB" style="margin: 5px;" runat="server" Text="Button" OnClick="Button1_Click" />            
                </div>
            </div>
        </div>
    </div>


</asp:Content>

