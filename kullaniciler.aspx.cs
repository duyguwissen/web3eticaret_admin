﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_kullaniciler : System.Web.UI.Page
{
        Uye uye = new Uye();

    protected void Page_Load(object sender, EventArgs e)
    {        
        Repeater1.DataSource = uye.UyeListe();
        Repeater1.DataBind();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
       //yetkilendirme
        LinkButton linkbtn = (LinkButton)sender;
        if (linkbtn.ToolTip.ToString() == "üye")
        {
            uye.UyeYetkilendir(Convert.ToInt32(linkbtn.CssClass), "asistan");
            Response.Redirect("kullaniciler.aspx");
        }
        else 
        {
            uye.UyeYetkilendir(Convert.ToInt32(linkbtn.CssClass), "üye");
            Response.Redirect("kullaniciler.aspx");
        }
            
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        //silme
        LinkButton linkbtn = (LinkButton)sender;
        int islem = uye.UyeSil(Convert.ToInt32(linkbtn.CssClass));
    }
}