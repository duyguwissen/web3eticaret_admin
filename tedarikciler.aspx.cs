﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_tedarikciler : System.Web.UI.Page
{
    Tedarikci tedarikci = new Tedarikci();

    protected void Page_Load(object sender, EventArgs e)
    {
        Repeater1.DataSource = tedarikci.TedarikciListe();
        Repeater1.DataBind();
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        LinkButton linkbtn = (LinkButton)sender;
        if (linkbtn.CssClass.ToString() == "False")
        {
            tedarikci.TedarikciAktifPasif(Convert.ToInt32(linkbtn.ToolTip), "True");
            Response.Redirect("tedarikciler.aspx");
        }
        else
        {
            tedarikci.TedarikciAktifPasif(Convert.ToInt32(linkbtn.ToolTip), "False");
            Response.Redirect("tedarikciler.aspx");
        }
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        LinkButton linkbtn = (LinkButton)sender;
        int islem = tedarikci.TedarikciSil(Convert.ToInt32(linkbtn.CssClass));
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton linkbtn = (LinkButton)sender;
        //int islem = tedarikci.TedarikciGetir(Convert.ToInt32(linkbtn.CssClass));
        int id = Convert.ToInt32(linkbtn.CssClass);
        Response.Redirect("tedarikciduzenle.aspx?tedarikciId="+id+"");
    }
}