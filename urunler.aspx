﻿<%@ Page Title="" Language="C#" MasterPageFile="Admin.master" AutoEventWireup="true" CodeFile="urunler.aspx.cs" Inherits="admin_urunler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="wrapper">
    <!-- Dynamic table -->
        <div class="widget">
            <div class="title"><img src="images/icons/dark/full2.png" alt="" class="titleIcon" /><h6>Ürünler</h6></div>                          
            <table cellpadding="0" cellspacing="0" border="0" class="display dTable">
            <thead>
            <tr>
                <th>Ürün Adı</th>
                <th>Kategori</th>
                <th>Stok</th>
                <th>Durum</th>
                <th>İşlem</th>
            </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <tr class="grade">
                            <td><%#Eval("urunAd") %></td>
                            <td><%#Eval("kategoriAd") %></td>
                            <td>sdffdfdsfsdf</td>
                            <td><%#Eval("urunAktifPasif") %></td>
                            <td class="actBtns">
                            
                                    <asp:LinkButton ID="LinkButton2" runat="server" class="tipS" original-title="Aktif/Pasif" ToolTip='<%#Eval("id") %>'  CssClass='<%#Eval("urunAktifPasif") %>'   OnClick="LinkButton2_Click">
                                         <img src='images/icons/<%#Eval("urunAktifPasif").ToString()=="True"?"taskProgress.png":"taskDone.png" %>' alt="">
                                    </asp:LinkButton>
                            
                                <a href="urundetay.aspx?urunid=<%#Eval("id") %>" class="tipS" original-title="Ürün Detayı">
                                    <img src="images/icons/edit.png" alt="">
                                </a>

                                <asp:LinkButton ID="LinkButton1" runat="server" class="tipS" original-title="Sil" CssClass='<%#Eval("id") %>'    OnClick="LinkButton1_Click">             
                                    <img src="images/icons/remove.png" alt="">
                                </asp:LinkButton> 
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
            </table>  
        </div>
        </div>
</asp:Content>

