﻿<%@ Page Title="" Language="C#" MasterPageFile="Admin.master" AutoEventWireup="true" CodeFile="tedarikciler.aspx.cs" Inherits="admin_tedarikciler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <!-- Main content wrapper -->
    <div class="wrapper">
        <a href="tedarikcikayit.aspx" title="" class="bFirst button basic"><img src="images/icons/dark/photos.png" class="icon" alt="" /><span>Yeni Tedarikçi Ekle</span></a>
    <!-- Static table -->
       <div class="widget">
            <div class="title"><img src="images/icons/dark/full2.png" alt="" class="titleIcon" /><h6>Tedarikçi Bilgileri - İşlemleri</h6></div>                          
            <table cellpadding="0" cellspacing="0" border="0" class="display dTable">
            <thead>
                <tr>
                    <th>Firma Adı</th>
                    <th>Adresi</th>
                    <th>Vergi No</th>
                    <th>İş Telefonu</th>
                    <th>Cep Telefonu</th>
                    <th>E-Mail</th>
                    <th>Tedarikçi A/P</th>
                    <th>İşlem</th>
                </tr>
            </thead>
            <tbody>
                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td><%#Eval("firmaAdi") %></td>
                                <td><%#Eval("adresDetay") %></td>
                                <td><%#Eval("vergiNo") %></td>
                                <td><%#Eval("isTel") %></td>
                                <td><%#Eval("cepTel") %></td>
                                <td><%#Eval("email") %></td>
                                <td><%#Convert.ToInt32(Eval("tAktifPasif"))==1?"Aktif":"Pasif" %></td>
                                <td class="actBtns">

                                    <asp:LinkButton ID="LinkButton3" runat="server" class="tipS" original-title="Aktif/Pasif" ToolTip='<%#Eval("ID") %>'  CssClass='<%#Eval("tAktifPasif") %>' OnClick="LinkButton3_Click">
                                         <img src='images/icons/<%#Eval("tAktifPasif").ToString()=="True"?"taskProgress.png":"taskDone.png" %>' alt="">
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="LinkButton1" runat="server" class="tipS" original-title="Düzenle" CssClass='<%#Eval("ID") %>' OnClick="LinkButton1_Click">
                                        <img src='images/icons/color/pencil.png' alt="">
                                   </asp:LinkButton>
                                    
                                    &nbsp;
                                    <asp:LinkButton ID="LinkButton2" runat="server" class="tipS" original-title="Sil" CssClass='<%#Eval("ID") %>' OnClick="LinkButton2_Click">
                                        <img src="images/icons/remove.png" alt="">
                                    </asp:LinkButton>
                                    &nbsp;&nbsp;
                            
                                </td>
                            </tr>
                        </ItemTemplate>

                    </asp:Repeater>
                </tbody>
            </table>  
        </div>
    
        
        <!-- Table with check all checkboxes fubction -->
        </div>
    <!--wrapper bitiş-->


</asp:Content>

