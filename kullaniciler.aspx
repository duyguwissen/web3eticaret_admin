﻿<%@ Page Title="" Language="C#" MasterPageFile="Admin.master" AutoEventWireup="true" CodeFile="kullaniciler.aspx.cs" Inherits="admin_kullaniciler" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

         <!-- Main content wrapper -->
    <div class="wrapper">
    <!-- Static table -->
       <div class="widget">
            <div class="title"><img src="images/icons/dark/full2.png" alt="" class="titleIcon" /><h6>Üye Bilgileri - İşlemleri</h6></div>                          
            <table cellpadding="0" cellspacing="0" border="0" class="display dTable">
            <thead>
            <tr>
                <th>Kullanıcı Adı</th>
                <th>Cep Telefonu</th>
                <th>E-Mail</th>
                <th>Yetki</th>
                <th>Son Giriş Tarihi</th>
                <th>İşlem</th>
            </tr>
            </thead>
            <tbody>
                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <tr class="gradeX">
                                <td><%#Eval("kullaniciAdi") %></td>
                                <td><%#Eval("cepTel") %></td>
                                <td><%#Eval("email") %></td>
                                <td><%#Eval("yetki") %></td>
                                <td><%#Eval("sonGirisTarihi") %></td>
                                <td class="actBtns">
                                       
                                    <asp:LinkButton ID="LinkButton1" runat="server" class="tipS" original-title="Yetkilendir" CssClass='<%#Eval("ID") %>' OnClick="LinkButton1_Click" ToolTip='<%#Eval("yetki").ToString().Trim() %>'>
                                        <img src='images/icons/dark/<%#Eval("yetki").ToString().Trim()=="asistan"? "adminUser.png":"user.png" %>' alt="">
                                   </asp:LinkButton>
                                    
                                    &nbsp;
                                    <asp:LinkButton ID="LinkButton2" runat="server" class="tipS" original-title="Sil" CssClass='<%#Eval("ID") %>' OnClick="LinkButton2_Click">
                                        <img src="images/icons/remove.png" alt="">
                                    </asp:LinkButton>
                            &nbsp;&nbsp;</td>
                            </tr>
                        </ItemTemplate>

                    </asp:Repeater>
                </tbody>
            </table>  
        </div>
    
        
        <!-- Table with check all checkboxes fubction -->
        </div>
    <!--wrapper bitiş-->

</asp:Content>

