﻿<%@ Page Title="" Language="C#" MasterPageFile="Admin.master" AutoEventWireup="true" CodeFile="profil.aspx.cs" Inherits="admin_profil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <div class="wrapper">
  <div class="widget">
            <div class="title"><img src="images/icons/dark/pencil.png" alt="" class="titleIcon" />
                <h6>Profil Bilgileri</h6></div>
			<form id="validate" method="post" action="submit.html" class="form">
                <fieldset class="step" id="w1first">
                   


                    <div class="userRow">
                            <a href="#" title=""><img src="images/user.png" alt="" class="floatL"></a>
                            <ul class="leftList">
                                <asp:Label ID="ad" runat="server" Text="Label"></asp:Label>
                            </ul>
                            
                            <div class="clear"></div>
                        </div>

                    <div class="formRow">
                        <label>TC:</label>
                        <div class="formRight"><input type="text" class="maskTC" value="" /></div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Sifre:</label>
                        <div class="formRight"><input type="password" name="sifre" id="pw" /></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label>Ad:</label>
                        <div class="formRight"><input type="text" name="ad" id="ad" /></div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Soyad:</label>
                        <div class="formRight"><input type="text" name="soyad" id="soyad" /></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label>Email :</label>
                        <div class="formRight"><input type="text" value="" name="emailValid" id="emailValid"></div><div class="clear"></div>
                    </div>
                  

                <div class="formRow">
                        <label>Dogum Tarihi:</label>
                        <div class="formRight"><input type="text" class="maskTin" value="" /></div>
                        <div class="clear"></div>
                    </div>
                     <div class="formRow">
                        <label>Cep Telefonu:</label>
                        <div class="formRight"><input type="text" class="maskPhone" value="" /></div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Ev Telefonu:</label>
                        <div class="formRight"><input type="text" class="maskPhone" value="" /></div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Cinsiyet:</label>
                        <div class="formRight">
                        	<div class="radio" id="uniform-radio1"><input type="radio" checked="checked" name="radio1" id="radio1" style="opacity: 0;"></div><label for="radio1">Kadin</label>
                            <div class="radio" id="uniform-radio2"><input type="radio" name="radio1" id="radio2" style="opacity: 0;"></div><label for="radio2">Erkek</label>
                            
                        </div><div class="clear"></div>
                    </div>



                    <div class="formRow">
                        <label>Your city:</label>
                        <div class="formRight"><input type="text" name="city" id="city" /></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label>Something more:</label>
                        <div class="formRight"><input type="text" name="smth" id="smth" /></div>
                        <div class="clear"></div>
                    </div>
                </fieldset>
				<div class="wizButtons"> 
                    <div class="status" id="status1"></div>
					<span class="wNavButtons">
                        <input class="basic" id="back1" value="Back" type="reset" />
                        <input class="blueB ml10" id="next1" value="Next" type="submit" />
                    </span>
				</div>
                <div class="clear"></div>
			</form>
			<div class="data" id="w1"></div>
        </div>
        </div>

    
</asp:Content>


