﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_kategoriler : System.Web.UI.Page
{
    public Kategori kategori = new Kategori();
    protected void Page_Load(object sender, EventArgs e)
    {
       
        Repeater1.DataSource = kategori.KategoriListe();
        Repeater1.DataBind();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton katbtn = (LinkButton)sender;
        kategori.KategoriSil(Convert.ToInt32(katbtn.ToolTip));
        Response.Redirect("kategoriler.aspx");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        int sonuc = kategori.KategoriEkle(TextBox1.Text, DropDownList1.SelectedValue.ToString());
        if (sonuc == 1)
        {
            Response.Write("<script>alert('İşlem Başarılı')</script>");
            Response.Redirect("kategoriler.aspx");
        }
        else
            Response.Write("<script>alert('İşlem Başarısız')</script>");
    }
}