﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mesajDetay : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Mesajlar mesaj = new Mesajlar();
        Repeater1.DataSource = mesaj.MesajlarListe();
        Repeater1.DataBind();
    }
}