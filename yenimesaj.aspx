﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="yenimesaj.aspx.cs" Inherits="yenimesaj" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="widget" style="margin-top: 70px; margin-left: 19px; margin-right: 20px;">
        <div class="title">
            <img src="images/icons/dark/list.png" alt="" class="titleIcon" />
            <h6>Yeni mesaj yaz</h6>
        </div>
        <asp:Label ID="Label1" runat="server"></asp:Label>
        <asp:Label ID="Label2" runat="server" BorderStyle="Ridge"></asp:Label>
        <div class="formRow">
            <label>Kullanıcı:</label>

            <div class="formRight">
                <asp:TextBox ID="txtKullanici" runat="server"></asp:TextBox>
            </div>
            <div class="clear"></div>
        </div>

        <div class="formRow">
            <label>E-Mail:</label>
            <div class="formRight">
                <asp:TextBox ID="txtEmail" runat="server" type="email"></asp:TextBox>
            </div>
            <div class="clear"></div>
        </div>

        <div class="formRow">
            <label>Mesaj:</label>
            <div class="formRight">
                <asp:TextBox ID="txtMesaj" Style="height: 250px" runat="server" TextMode="MultiLine" CssClass="autogrow lim" BorderStyle="Outset">

                </asp:TextBox>
            </div>
            <div class="clear"></div>
        </div>
        <div class="formRow">
            <asp:Button ID="Button1" runat="server" Text="Gönder" OnClick="Button1_Click" />
        </div>

    </div>
</asp:Content>

