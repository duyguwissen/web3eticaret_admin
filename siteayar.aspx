﻿<%@ Page Title="" Language="C#" MasterPageFile="Admin.master" AutoEventWireup="true" CodeFile="siteayar.aspx.cs" Inherits="admin_siteayar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div id="rightSide">    
    <!-- Main content wrapper -->
    <div class="wrapper">        
        <!-- Usual wizard with ajax -->
        <div class="widget">
            <div class="title"><img src="images/icons/dark/pencil.png" alt="" class="titleIcon"/>
                <h6>Siteayar 1. Kısım</h6>
            </div>
			<form id="wizard1" method="post" action="submit.html" class="form ui-formwizard">
                <fieldset class="step ui-formwizard-content" id="w1first" style="display: block;">
                    <h1></h1>
                    <div class="formRow">
                        <label>Alan1:</label>
                        <div class="formRight"><input type="text" name="username" id="username" class="ui-wizard-content"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label>Alan2:</label>
                        <div class="formRight"><input type="password" name="pw" id="pw" class="ui-wizard-content"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label>Alan3:</label>
                        <div class="formRight"><input type="text" name="mail" id="mail" class="ui-wizard-content"></div>
                        <div class="clear"></div>
                    </div>
                </fieldset>
                <div class="formSubmit"><input type="submit" value="submit" class="redB"></div>
                <div class="clear"></div>
			</form>
			<div class="data" id="w1"></div>
        </div>
        
        <!-- Wizard with custom fields validation -->
        <div class="widget">
            <div class="title"><img src="images/icons/dark/pencil.png" alt="" class="titleIcon"/><h6>Siteayar 2. Kısım</h6></div>
			<form id="wizard2" method="post" action="submit.html" class="form ui-formwizard" novalidate="novalidate">
                <fieldset class="step ui-formwizard-content" id="w2first" style="display: block;">
                    <h1></h1>
                    <div class="formRow">
                        <label>Alan1:<span class="req">*</span></label>
                        <div class="formRight"><input type="text" name="bazinga" class="ui-wizard-content"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label>Alan2:</label>
                        <div class="formRight"><input type="password" name="pw1" id="pw1" class="ui-wizard-content"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label>Alan3:<span class="req">*</span></label>
                        <div class="formRight"><input type="text" name="email" class="ui-wizard-content"></div>
                        <div class="clear"></div>
                    </div>
                </fieldset>
                <div class="formSubmit"><input type="submit" value="submit" class="redB"></div>
                <div class="clear"></div>
			</form>
			<div class="data" id="w2"></div>
        </div>
        
        <!-- Submit form without ajax -->
        <div class="widget">
            <div class="title"><img src="images/icons/dark/pencil.png" alt="" class="titleIcon"/><h6>Siteayar 3. Kısım</h6></div>
			<form id="wizard3" method="post" action="submit.html" class="form ui-formwizard">
                <fieldset class="step ui-formwizard-content" id="w3first" style="display: block;">
                    <h1></h1>
                    <div class="formRow">
                        <label>Alan1:</label>
                        <div class="formRight"><input type="text" name="country" class="ui-wizard-content"></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label>Alan2:</label>
                        <div class="formRight"><input type="password" name="pw2" id="pw2" class="ui-wizard-content"></div>
                        <div class="clear"></div>
                    </div>
                </fieldset>
                
                <div class="formSubmit"><input type="submit" value="submit" class="redB"></div>
                <div class="clear"></div>
			</form>
			<div class="data" id="w3"></div>
        </div> 
    </div>
</div>

</asp:Content>

