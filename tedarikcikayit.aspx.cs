﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class tedarikcikayit : System.Web.UI.Page
{
    il il = new il();
    ilce ilce = new ilce();
    VergiDairesi vd = new VergiDairesi();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        ddlIl.DataTextField = "il";
        ddlIl.DataValueField = "id";
        ddlIl.DataSource = il.IlListe();
        ddlIl.DataBind();

        int secilenIlId = Convert.ToInt32(ddlIl.SelectedValue.ToString());
        string secilenIl = ddlIl.SelectedItem.Text.ToString();
        Response.Write(secilenIl);

        ddlIlce.DataTextField = "ilce";//dropdownlistin text kısmına veritabanından gelen ilce kolonunu atıyor.
        ddlIlce.DataValueField = "id";
        ddlIlce.DataSource = ilce.IlceListe(secilenIlId);
        ddlIlce.DataBind();

        ddlVergi.DataTextField = "daire";
        ddlVergi.DataValueField = "id";
        ddlVergi.DataSource = vd.DaireListe(secilenIl);
        ddlVergi.DataBind();
        }

    }

    protected void ddlIl_SelectedIndexChanged(object sender, EventArgs e)
    {
        int secilenIlId = Convert.ToInt32(ddlIl.SelectedValue.ToString());
        string secilenIl = ddlIl.SelectedItem.Text.ToString();

        ddlIlce.DataTextField = "ilce";//dropdownlistin text kısmına veritabanından gelen ilce kolonunu atıyor.
        ddlIlce.DataValueField = "id";
        ddlIlce.DataSource = ilce.IlceListe(secilenIlId);
        ddlIlce.DataBind();

        ddlVergi.DataTextField = "daire";
        ddlVergi.DataValueField = "id";
        ddlVergi.DataSource = vd.DaireListe(secilenIl);
        ddlVergi.DataBind();
    }
}