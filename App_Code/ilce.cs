﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ilce
/// </summary>
public class ilce
{
    Data data;

	public ilce()
	{
       this.data = new Data();
	}

    public DataTable IlceListe(int id)
    {
        data.cmd.Parameters.AddWithValue("pid", id);
        return data.TableGetir("SELECT * FROM tbl_ilce WHERE ilID=@pid");
    }
}