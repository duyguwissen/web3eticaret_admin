﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Tedarikci
/// </summary>
public class Tedarikci
{
    public int ID { get; set; }
    public string firmaAdi { get; set; }
    public int ilID { get; set; }
    public int ilceID { get; set; }
    public string adresDetay { get; set; }
    public int vergiDairesiID { get; set; }
    public string vergiNo { get; set; }
    public string isTel { get; set; }
    public string cepTel { get; set; }
    public string email { get; set; }

    Data data;

	public Tedarikci()
	{
        this.data = new Data();
	}

    public DataTable TedarikciListe() {
        return data.TableGetir("SELECT * FROM tbl_tedarikci");
    }

    public DataRow TedarikciGetir(int id)
    {
        data.cmd.Parameters.AddWithValue("pid",id);
        return data.SatirGetir("SELECT * FROM tbl_tedarikci WHERE ID=@pid");
    }

    public int TedarikciAktifPasif(int id, string deger)
    {
        data.cmd.Parameters.AddWithValue("tid", id);
        data.cmd.Parameters.AddWithValue("deger", deger);
        return data.ExecuteNonQuery("UPDATE tbl_tedarikci SET tAktifPasif=@deger WHERE ID=@tid");
    }

    public int TedarikciSil(int id)
    {
        data.cmd.Parameters.AddWithValue("pid", id);
        return data.ExecuteNonQuery("DELETE FROM tbl_tedarikci WHERE ID=@pid");
    }

    public void TedarikciEkle(int id, string fadi, int ilid, int ilceid, string adetay, int vdid, string vno, string istel, string ceptel, string email)
    { 
        data.cmd.Parameters.AddWithValue("fad", fadi);
        data.cmd.Parameters.AddWithValue("ilid", ilid);
        data.cmd.Parameters.AddWithValue("ilceid", ilceid);
        data.cmd.Parameters.AddWithValue("adetay", adetay);
        data.cmd.Parameters.AddWithValue("vdid", vdid);
        data.cmd.Parameters.AddWithValue("vno", vno);
        data.cmd.Parameters.AddWithValue("istel", istel);
        data.cmd.Parameters.AddWithValue("ceptel", ceptel);
        data.cmd.Parameters.AddWithValue("email", email);
        data.ExecuteNonQuery("INSERT INTO tbl_tedarikci (firmaAdi, ilID, ilceID, adresDetay, vergiDairesiID, vergiNo, isTel, cepTel, email) VALUES (@fad, @ilid, @ilceid, @adetay, @vdid, @vno, @istel, @ceptel, @email)");
       
    }

    public int TedarikciDuzenle(int id, string fadi, int ilid, int ilceid, string adetay, int vdid, string vno, string istel, string ceptel, string email) {
        data.cmd.Parameters.AddWithValue("pid",id);
        data.cmd.Parameters.AddWithValue("fad", fadi);
        data.cmd.Parameters.AddWithValue("ilid", ilid);
        data.cmd.Parameters.AddWithValue("ilceid", ilceid);
        data.cmd.Parameters.AddWithValue("adetay", adetay);
        data.cmd.Parameters.AddWithValue("vdid", vdid);
        data.cmd.Parameters.AddWithValue("vno", vno);
        data.cmd.Parameters.AddWithValue("istel", istel);
        data.cmd.Parameters.AddWithValue("ceptel", ceptel);
        data.cmd.Parameters.AddWithValue("email", email);
        return data.ExecuteNonQuery("UPDATE tbl_tedarikci SET firmaAdi=@fad ilID=@ilid ilceID=@ilceid adresDetay=@adetay vergiDairesiID=@vdid vergiNo=@vno isTel=@istel cepTel=@ceptel email=@email WHERE ID=@pid");
    }

}