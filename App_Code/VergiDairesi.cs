﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;

/// <summary>
/// Summary description for VergiDairesi
/// </summary>
public class VergiDairesi
{
    int id { get; set; }
    string il { get; set; }
    string ilce { get; set; }
    string daire { get; set; }

    Data data;

	public VergiDairesi()
	{
		this.data=new Data();
	}

    public DataTable DaireListe(string il)
    {
        data.cmd.Parameters.AddWithValue("pil", il);
        return data.TableGetir("SELECT * FROM tbl_vergiDairesi WHERE il=@pil");
    }
}