﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Kategori
/// </summary>
public class Kategori
{
    //urun tablosundaki alanlar
    int id { get; set; }
    int urunKategoriId { get; set; }
    int urunMarkaId { get; set; }
    string urunAd { get; set; }
    string urunKod { get; set; }
    double urunFiyat { get; set; }
    int urunStok { get; set; }
    bool urunAktifPasif { get; set; }
    string urunAciklama { get; set; }
    bool urunCinsiyet { get; set; }
    
    
    //urunDetay tablosundaki alanlar
    int urunDetayId { get; set; }
    int urunId { get; set; }
    int ozellikId { get; set; }
    int ozellikDetayId { get; set; }


    //urunResim tablosundaki alanlar
    int resimId { get; set; }
    int urunID { get; set; }
    string resimUrl { get; set; }


    Data db { get; set; }

	public Kategori()
	{
        this.db = new Data();
	}
    public DataTable KategoriDropDown() {
        return db.TableGetir("SELECT * FROM tbl_kategori");
    }
    public DataTable KategoriListe(){
        //return db.TableGetir("SELECT id,k.kategoriAd,k.kategoriId,k.kategoriCinsiyet,COUNT(u.urunKategoriId) as UrunSayısı FROM tbl_urun u INNER JOIN tbl_kategori k ON u.urunKategoriId=k.kategoriId GROUP BY k.kategoriAd,id,k.kategoriId,k.kategoriCinsiyet");
        return db.TableGetir("SELECT k.kategoriAd,k.kategoriId,k.kategoriCinsiyet, (SELECT COUNT(*) FROM tbl_urun u WHERE u.urunKategoriId=k.kategoriId) AS UrunSayısı FROM tbl_kategori k GROUP BY k.kategoriAd,k.kategoriId,k.kategoriCinsiyet");
    }

    public int KategoriSil(int katid) {
        db.cmd.Parameters.AddWithValue("katid", katid);
        return db.ExecuteNonQuery("DELETE FROM tbl_kategori WHERE kategoriId=@katid");
    }

    public int KategoriEkle(string ad,string cins) {
        db.cmd.Parameters.AddWithValue("ad", ad);
        db.cmd.Parameters.AddWithValue("cins", cins);
        return db.ExecuteNonQuery("INSERT INTO tbl_kategori (kategoriAd,kategoriCinsiyet) VALUES(@ad,@cins)");
    }
}