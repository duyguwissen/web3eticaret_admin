﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Urun
/// </summary>
public class Urun
{
    //urun tablosundaki alanlar
    int id { get; set; }
    int urunKategoriId { get; set; }
    int urunMarkaId { get; set; }
    string urunAd { get; set; }
    string urunKod { get; set; }
    double urunFiyat { get; set; }
    int urunStok { get; set; }
    bool urunAktifPasif { get; set; }
    string urunAciklama { get; set; }
    bool urunCinsiyet { get; set; }
    
    
    //urunDetay tablosundaki alanlar
    int urunDetayId { get; set; }
    int urunId { get; set; }
    int ozellikId { get; set; }
    int ozellikDetayId { get; set; }


    //urunResim tablosundaki alanlar
    int resimId { get; set; }
    int urunID { get; set; }
    string resimUrl { get; set; }


    Data db { get; set; }

	public Urun()
	{
        this.db = new Data();
	}

    public DataTable UrunListe(){
        return db.TableGetir("SELECT * FROM tbl_urun U INNER JOIN tbl_kategori K ON U.urunKategoriId=K.kategoriId WHERE silinmismi='False'");
    }

    public int UrunSil(int urunid) {
        db.cmd.Parameters.AddWithValue("deger",1);
        db.cmd.Parameters.AddWithValue("urunid", urunid);
        return db.ExecuteNonQuery("UPDATE tbl_urun SET silinmismi=@deger WHERE id=@urunid");
    }
    public int UrunAktifPasif(int id,string deger) {
        db.cmd.Parameters.AddWithValue("urunid",id);
        db.cmd.Parameters.AddWithValue("deger",deger);
        return db.ExecuteNonQuery("UPDATE tbl_urun SET urunAktifPasif=@deger WHERE id=@urunid");
    }

    public DataRow UrunGetir(int urunid) {
        db.cmd.Parameters.AddWithValue("urunid", urunid);
        return db.SatirGetir("SELECT * FROM tbl_urun WHERE id=@urunid");
    }

    public int UrunBilgiDuzenle(int id, string ad, int katid, double fiyat, int birim, int stok, string aciklama) {
        db.cmd.Parameters.AddWithValue("urunid", id);
        db.cmd.Parameters.AddWithValue("ad", ad);
        db.cmd.Parameters.AddWithValue("katid", katid);
        db.cmd.Parameters.AddWithValue("fiyat", fiyat);
        db.cmd.Parameters.AddWithValue("birim", birim);
        db.cmd.Parameters.AddWithValue("stok", stok);
        db.cmd.Parameters.AddWithValue("aciklama", aciklama);

        return db.ExecuteNonQuery(@"UPDATE tbl_urun SET    
                                    urunKategoriId=@katid,
                                    urunAd=@ad,
                                    urunFiyat=@fiyat,
                                    urunParaBirimiId=@birim,
                                    urunStok=@stok,
                                    urunAciklama=@aciklama
                                    WHERE id=@urunid");
    }
}