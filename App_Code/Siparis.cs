﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Siparis
/// </summary>
public class Siparis
{
    //siparis tablosu alanları
    int siparisId { get; set; }
    int urunId { get; set; }
    int urunAdet { get; set; }
    double toplamTutar { get; set; }
    int uyeId { get; set; }
    DateTime siparisTarih { get; set; }
    int siparisDurumId { get; set; }
    int odemeTipId { get; set; }
    int kargoId { get; set; }
    DateTime ongorulenTeslimTarih { get; set; }
    DateTime teslimTarih { get; set; }
    string takipNo { get; set; }

    //siparisDurum tablosu alanları
    //int siparisDurumId { get; set; }
    string siparisDurum { get; set; }


	public Siparis()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}