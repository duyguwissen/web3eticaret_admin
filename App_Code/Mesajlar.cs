﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for mesajlar
/// </summary>
public class Mesajlar
{
    //tbl_mesajlar tablosundaki alanlar
    public int  ID { get; set; }
    public int GonderenID {get; set; }
    public int AliciID {get; set; }
    public string Kullanici { get; set; }
    public string Mesaj {get; set; }
    public string Email { get; set; }
    public DateTime Tarih {get; set; }
    public string Islem { get; set; }

    //tbl_mesajDetay tablosundaki alanlar
    int tblMesajID{ get; set; }
    int mesajID { get; set; }

    public Data db { get; set; }

    Data data;

	public Mesajlar()
	{
        this.db = new Data();
        data = new Data();
	}

    public DataTable MesajlarListe()
    {
        return db.TableGetir("SELECT * FROM tbl_mesajlar AS Kullanici");
    }

    public void MesajGonder()
    {
        data.cmd.Parameters.AddWithValue("pgonderenId",GonderenID);
        data.cmd.Parameters.AddWithValue("paliciId", AliciID);
        data.cmd.Parameters.AddWithValue("pkullanici", this.Kullanici);
        data.cmd.Parameters.AddWithValue("pmesaj", this.Mesaj);
        data.cmd.Parameters.AddWithValue("pemail", this.Email);
        DataRow dr = data.SatirGetir(@"INSERT INTO tbl_mesajlar (GonderenID,AliciID,Kullanici,Mesaj,Email) 
            VALUES (@pgonderenId,@paliciId,@pkullanici,@pmesaj,@pemail)");

        if (dr != null)
        {
            this.AliciID = Convert.ToInt32(dr["paliciId"]);
            this.GonderenID = Convert.ToInt32(dr["pgonderenId"]);
        }
    }

    public int MesajlarSil(int id)
    {
        data.cmd.Parameters.AddWithValue("pid", id);
        return data.ExecuteNonQuery("DELETE FROM tbl_mesajlar WHERE ID=@pid");
    }
}