﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Ozellik
/// </summary>
public class Ozellik
{
    // ozellik tablosu alanları
    int ozellikId { get; set; }
    string ozellikAd { get; set; }


    // ozellikDetay tablosu alanları
    int ozellikDetayId { get; set; }
    //int ozellikId { get; set; }
    string ozellikDetayi { get; set; }

    Data db;
	public Ozellik()
	{
        db = new Data();
	}


    public DataTable TumOzellik()
    {
        return db.TableGetir("SELECT * FROM tbl_ozellik ");
    }

    public DataTable UrunOzellik(int id) {
        db.cmd.Parameters.AddWithValue("id", id);
        return db.TableGetir("SELECT DISTINCT O.ozellikAdi,O.ozellikId,O.ozellikAdi FROM tbl_ozellik O INNER JOIN tbl_urunDetay UD ON O.ozellikId=UD.ozellikId WHERE UD.urunId=@id");
    }

    public DataTable UrunOzellikDetay(int urunid , int ozid) {
        db.cmd.Parameters.AddWithValue("ozid",ozid);
        db.cmd.Parameters.AddWithValue("urunid", urunid);
        return db.TableGetir("SELECT * FROM tbl_ozellikDetay OD INNER JOIN tbl_ozellik O ON OD.ozellikId=O.ozellikId INNER JOIN tbl_urunDetay UD ON UD.ozellikDetayId=OD.ozellikDetayId WHERE UD.urunId=@urunid AND UD.ozellikId=@ozid");
    }
    public int UrunOzellikDetaySil(int ozdetayid) {
        db.cmd.Parameters.AddWithValue("ozdetayid", ozdetayid);
        return db.ExecuteNonQuery("DELETE FROM tbl_ozellikDetay WHERE ozellikDetayId=@ozdetayid");
    }
}