﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Kampanya
/// </summary>
public class Kampanya
{
    int ID { get; set; }
    int urunID { get; set; }
    double urunOrani { get; set; }
    DateTime baslangicTarihi { get; set; }
    DateTime bitisTarihi { get; set; }
    string kampanyaBaslik { get; set; }
    string kampanyaAciklama { get; set; }


	public Kampanya()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}