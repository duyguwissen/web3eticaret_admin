﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Uye
/// </summary>
public class Uye
{
    //uye tablosu alanları
    public int ID { get; set; }
    public string TC { get; set; }
    public string kullaniciAdi { get; set; }
    public string sifre { get; set; }
    public string ad { get; set; }
    public string soyad { get; set; }
    public bool cinsiyet { get; set; }
    public DateTime dogumTarihi { get; set; }
    public string cepTel { get; set; }
    public string evTel { get; set; }
    public string email { get; set; }
    public string yetki { get; set; }
    public DateTime kayitTarihi { get; set; }


    //adres tablosu alanları
    public int uyeID { get; set; }
    public string adresTanimi { get; set; }
    public string adSoyad { get; set; }
    public int sehirID { get; set; }
    public int ilceId { get; set; }
    public string adresDetay { get; set; }
    public string postaKodu { get; set; }
    //string cepTel { get; set; }
    //string evTel { get; set; }
    Data data;

    public Uye()
    {
        this.data = new Data();
    }

    public void GirisYap(string kullaniciAdi, string sifre)
    {
        data.cmd.Parameters.AddWithValue("pkadi", kullaniciAdi);
        data.cmd.Parameters.AddWithValue("psifre", sifre);
        DataRow dr = data.SatirGetir("SELECT * FROM tbl_uye WHERE kullaniciAdi=@pkadi AND sifre=@psifre");

        //    if (dr != null) HttpContext.Current.Session["login"] = dr;

        if (dr != null)
        {
            this.kullaniciAdi = dr["kullaniciAdi"].ToString();
            this.sifre = dr["sifre"].ToString();
            this.yetki = dr["yetki"].ToString();
            this.ID = Convert.ToInt32(dr["ID"]);
        }
    }

    public DataTable UyeListe()
    {
        return data.TableGetir("SELECT * FROM tbl_uye WHERE yetki NOT LIKE '%admin%'");
    }

    public int UyeSil(int id)
    {
        data.cmd.Parameters.AddWithValue("pid", id);
        return data.ExecuteNonQuery("DELETE FROM tbl_uye WHERE ID=@pid");
    }

    public int UyeYetkilendir(int id, string yetki )
    {
        data.cmd.Parameters.AddWithValue("pyetki", yetki);
        data.cmd.Parameters.AddWithValue("pid", id);
        return data.ExecuteNonQuery("UPDATE tbl_uye SET yetki=@pyetki WHERE ID=@pid");
    }

    public DataTable TumUyeler()
    {
        return data.TableGetir("Select * From tbl_uye");
    }

    public DataRow UyeGetir(int uyeid)
    {
        data.cmd.Parameters.AddWithValue("uyeid", uyeid);
        return data.SatirGetir("SELECT * FROM tbl_uye WHERE ID=@uyeid");
    }
}