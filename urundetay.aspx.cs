﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class urundetay : System.Web.UI.Page
{
    public Urun urun = new Urun();
    public Kategori kategori = new Kategori();
    public SiteAyar siteayar = new SiteAyar();
    public Ozellik ozellik = new Ozellik();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Request.QueryString["urunid"]!=null & !IsPostBack){
        int urunid=Convert.ToInt32(Request.QueryString["urunid"]);
        DataRow degerler = urun.UrunGetir(urunid);
        Label1.Text = degerler["urunEklenmeTarih"].ToString();
        Label2.Text = degerler["urunKod"].ToString();
        urunAd.Text = degerler["urunAd"].ToString();
        urunFiyat.Text = degerler["urunFiyat"].ToString();
      
        urunAciklama.Text = degerler["urunAciklama"].ToString();
        DropDownList1.SelectedValue = degerler["urunKategoriId"].ToString();
        DropDownList2.SelectedValue = degerler["urunParaBirimiId"].ToString();
        //Ürünün Özellik Bilgileri
        Repeater1.DataSource = ozellik.UrunOzellik(urunid);
        Repeater1.DataBind();
        //Ürünün Özellik Detay Bilgileri
        
        }
        //Kategori DropDownList
        DataTable kategoriDrop = kategori.KategoriDropDown();
        DropDownList1.DataTextField = "kategoriAd";
        DropDownList1.DataValueField = "kategoriId";
        DropDownList1.DataSource = kategoriDrop;
        DropDownList1.DataBind();
        //ParaBirimi DropDownList
        DataTable parabirimDrop = siteayar.ParaBirimleri();
        DropDownList2.DataTextField = "paraBirimiAd";
        DropDownList2.DataValueField = "paraBirimiID";
        DropDownList2.DataSource = parabirimDrop;
        DropDownList2.DataBind();
        //Ozellik DropDownList
        DataTable ozelliklerDrop = ozellik.TumOzellik();
        DropDownList3.DataTextField = "ozellikAdi";
        DropDownList3.DataValueField = "ozellikId";
        DropDownList3.DataSource = ozelliklerDrop;
        DropDownList3.DataBind();
    }
   
    protected void Button1_Click(object sender, EventArgs e)
    {
        int urunid = Convert.ToInt32(Request.QueryString["urunid"]);
        int islem = urun.UrunBilgiDuzenle(urunid,urunAd.Text,Convert.ToInt32(DropDownList1.SelectedValue),Convert.ToDouble(urunFiyat.Text),Convert.ToInt32(DropDownList2.SelectedValue),Convert.ToInt32(urunStok.Text),urunAciklama.Text);
        if (islem == 1)
            Response.Write("<script>alert('İşlem Başarılı');</script>");
        else
            Response.Write("<script>alert('İşlem Başarısız');</script>");
    }

    protected void Repeater1_ItemDataBound1(object sender, RepeaterItemEventArgs e)
    {
        if (Request.QueryString["urunid"] != null & !IsPostBack)
        {
            Label ozellikLbl = (Label)e.Item.FindControl("ozellik");
            Repeater rpt = (Repeater)e.Item.FindControl("Repeater2");
            int urunid = Convert.ToInt32(Request.QueryString["urunid"]);
            int ozellikid = Convert.ToInt32(ozellikLbl.ToolTip);
            rpt.DataSource = ozellik.UrunOzellikDetay(urunid,ozellikid);
            rpt.DataBind();
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        ozellik.UrunOzellikDetaySil(Convert.ToInt32(lnkbtn.CssClass));
    }
}