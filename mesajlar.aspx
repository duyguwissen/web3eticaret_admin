﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="mesajlar.aspx.cs" Inherits="mesajlar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="wrapper">
        <div class="pageTitle">
                <h5>MESAJLAR</h5>                  
         </div>
    
    </div>
    <form action="" class="form1">
            <fieldset>
                <div class="widget" style="margin-top: 70px; clear: both; margin-left: -93px; margin-right: 20px;">
                    <div class="title"><img src="images/icons/dark/full2.png" alt="" class="titleIcon" />
                        <h6>Mesajları Görüntüle</h6></div>                          
              
        <table cellpadding="0" cellspacing="0" border="0" class="display dTable">
            <thead>
            <tr>
                <th>Gönderen Kişi</th>
                <th>Alıcı</th>
                <th>Kullanıcı</th>
                <th>Mesaj</th>
                <th>Email</th>
                <th>Tarih</th>
                <th>İşlem</th>

            </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <tr class="gradeX">
                            <td><%#Eval("GonderenID") %></td>
                            <td><%#Eval("AliciID") %></td>
                            <td><%#Eval("Kullanici") %></td>
                            <td><%#Eval("Mesaj") %></td>
                            <td><%#Eval("Email") %></td>
                            <td><%#Eval("Tarih") %></td>
                            <td class="actBtns">     
                                <asp:LinkButton ID="LinkButton1" runat="server" class="tipS" original-title="Sil" CssClass='<%#Eval("ID") %>' OnClick="LinkButton1_Click">
                                    <img src="images/icons/remove.png" alt="">
                                </asp:LinkButton>
                           </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
            </table>  
                </div>
            
        <div class="widget" style="margin-top: 70px; margin-left: -93px; margin-right: 20px;">
            <div class="title"><img src="images/icons/dark/list.png" alt="" class="titleIcon" />
                <h6>Mail At</h6></div>
                <asp:Label ID="Label1" runat="server" ></asp:Label>
                <asp:Label ID="Label2" runat="server" BorderStyle="Ridge"></asp:Label>  
                <div class="formRow">                        
                            <label>Kullanıcı:</label>
          
                            <div class="formRight">
                                 <asp:DropDownList ID="ddlKullanici" runat="server" Visible="True" >
                                        
                                  </asp:DropDownList>
                            </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>E-Mail:</label>                                  
                        <div class="formRight">
                            <asp:TextBox ID="txtEmail" runat="server" type="email"></asp:TextBox>
                        </div>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="formRow">
                        <label>Mesaj:</label>
                        <div class="formRight">
                            <asp:TextBox ID="txtMesaj" runat="server" TextMode="MultiLine" CssClass="autogrow lim"></asp:TextBox></div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <asp:Button ID="Button1" runat="server" Text="Gönder" OnClick="Button1_Click" />
                    </div>
                    
                </div>
        </fieldset>  
    </form>
</asp:Content>

