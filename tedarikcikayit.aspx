﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="tedarikcikayit.aspx.cs" Inherits="tedarikcikayit" EnableEventValidation="False" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="wrapper">

        <!-- Form -->
        <form action="tedarikciler.aspx" class="form">
            <fieldset>
                <div class="widget">
                    <div class="title">
                        <img src="images/icons/dark/list.png" alt="" class="titleIcon" /><h6>Tedarikçi Ekle</h6>
                    </div>

                    <div class="formRow">
                        <label>Tedarikçi Adı:</label>
                        <div class="formRight">
                            <input type="text" value="" />
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>İl Seçiniz:</label>
                        <div class="formRight searchDrop">
                     
                            <asp:DropDownList ID="ddlIl" runat="server" class="chzn-select" Style="width: 350px;" TabIndex="2" AutoPostBack="True" OnSelectedIndexChanged="ddlIl_SelectedIndexChanged" > 
                                
                                </asp:DropDownList>
                       
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>İlçe Seçiniz:</label>
                        <div class="formRight searchDrop">
                            <asp:DropDownList ID="ddlIlce" runat="server" class="chzn-select" style="width: 350px;" tabindex="2"></asp:DropDownList>

                            <%--<select data-placeholder="İlçe seçiniz..." class="chzn-select" style="width: 350px;" tabindex="2" name='<%#Eval("id") %>'>
                                <asp:Repeater ID="rptIlce" runat="server">
                                    <ItemTemplate>
                                        <option value=""></option>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </select>--%>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Adres:</label>
                        <div class="formRight">
                            <textarea rows="8" cols="" name="textarea"></textarea>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Vergi Dairesi Seçiniz:</label>
                        <div class="formRight searchDrop">
                            <asp:DropDownList ID="ddlVergi" runat="server" class="chzn-select" style="width: 350px;" tabindex="2"></asp:DropDownList>
                         <%--   <select data-placeholder="Vergi Dairesi Seçiniz..." class="chzn-select" style="width: 350px;" tabindex="2">
                                <option value=""></option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="Canada">Canada</option>
                                <option value="Cape Verde">Cape Verde</option>
                                <option value="Cayman Islands">Cayman Islands</option>
                            </select>--%>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Vergi Numarası:</label>
                        <div class="formRight">
                            <input type="text" value="" />
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>İş Telefonu:</label>
                        <div class="formRight">
                            <input type="text" value="" />
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Cep Telefonu:</label>
                        <div class="formRight">
                            <input type="text" value="" />
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>E-Mail:</label>
                        <div class="formRight">
                            <input type="text" value="" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <div class="formSubmit">
                            <input type="submit" value="EKLE" class="redB" />
                        </div>
                        <div class="clear"></div>
                    </div>

                </div>
            </fieldset>


        </form>

    </div>
</asp:Content>

