﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mesajlar : System.Web.UI.Page
{
    Mesajlar gmesaj = new Mesajlar();
    protected void Page_Load(object sender, EventArgs e)
    {
        Mesajlar mesaj = new Mesajlar();
        Repeater1.DataSource = mesaj.MesajlarListe();
        Repeater1.DataBind();

        Uye uye = new Uye();
        ddlKullanici.DataSource = uye.TumUyeler();
        ddlKullanici.DataValueField = "ID";
        ddlKullanici.DataTextField = "KullaniciAdi";
        ddlKullanici.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Mesajlar mesajgonder = new Mesajlar();
        mesajgonder.AliciID = Convert.ToInt32(ddlKullanici.SelectedValue);
        mesajgonder.Kullanici = Convert.ToString(ddlKullanici.SelectedItem);

        if (mesajgonder.GonderenID != null)
        {
            if (mesajgonder.ID.Trim().ToLower())
            {
                Session["kontrol"] = mesajgonder;
                Response.Redirect("/default.aspx");
            }
            else
            {
                Response.Write("<script>alert('Böyle bir kullanıcı yoktur.')</script>");
            }
        }
        
        mesajgonder.Mesaj = txtMesaj.Text;
        mesajgonder.Email = txtEmail.Text;
        mesajgonder.MesajGonder();



        MailMessage ePosta = new MailMessage();
        ePosta.From = new MailAddress("web3eticaret.deneme@gmail.com");
        ePosta.To.Add(txtEmail.Text);
        ePosta.Subject = "Deneme";
        ePosta.Body = txtMesaj.Text;
        SmtpClient smtp = new SmtpClient();
        smtp.Credentials = new NetworkCredential("web3eticaret.deneme@gmail.com", "web3deneme");
        smtp.Port = 587;
        smtp.Host = "smtp.gmail.com";
        smtp.EnableSsl = true;
        try
        {
            Response.Write(txtEmail.Text);
            Response.Write("<script>alert('E-Mail adresinize gönderilmiştir. Teşekkür ederiz !');</script>");
        }
        catch
        {
            Response.Write(txtEmail.Text);
            Response.Write("<script>alert('Mesaj Gönderilirken bir hata oluştu.'</script>");
        }
        smtp.Send(ePosta);
    }


    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton lnkbtn = (LinkButton)sender;
        int islem = gmesaj.MesajlarSil(Convert.ToInt32(lnkbtn.CssClass));
    }
}