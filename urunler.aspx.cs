﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_urunler : System.Web.UI.Page
{
    Urun urun = new Urun();
    protected void Page_Load(object sender, EventArgs e)
    {
        Repeater1.DataSource = urun.UrunListe();
        Repeater1.DataBind();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton linkbtn = (LinkButton) sender;
        int islem = urun.UrunSil(Convert.ToInt32(linkbtn.CssClass));
        Response.Redirect("urunler.aspx");
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        LinkButton linkbtn = (LinkButton)sender;
        if (linkbtn.CssClass.ToString() == "False")
        {
            urun.UrunAktifPasif(Convert.ToInt32(linkbtn.ToolTip), "True");
            Response.Redirect("urunler.aspx");
        }
        else
        {
            urun.UrunAktifPasif(Convert.ToInt32(linkbtn.ToolTip), "False");
            Response.Redirect("urunler.aspx");
        }
    }
}