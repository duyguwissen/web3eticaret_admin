﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="tedarikciduzenle.aspx.cs" Inherits="tedarikciduzenle" EnableEventValidation="False" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="wrapper">

        <!-- Form -->
        <form action="tedarikciler.aspx" class="form">
            <fieldset>
                <div class="widget">
                    <div class="title">
                        <img src="images/icons/dark/list.png" alt="" class="titleIcon" /><h6>Tedarikçi Düzenle</h6>
                    </div>

                    <div class="formRow">
                        <label>Tedarikçi Adı:</label>
                        <div class="formRight">
                            <%--<input type="text" value="" />--%>
                            <asp:TextBox ID="txtTedarikciAd" runat="server"></asp:TextBox>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>İl Seçiniz:</label>
                        <div class="formRight searchDrop">
                            <asp:DropDownList ID="ddlIl" runat="server"  class="chzn-select" style="width: 350px;" AutoPostBack="true" OnSelectedIndexChanged="ddlIl_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>İlçe Seçiniz:</label>
                        <div class="formRight searchDrop">
                            <asp:DropDownList ID="ddlIlce"  class="chzn-select" style="width: 350px;" runat="server"></asp:DropDownList>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Adres:</label>
                        <div class="formRight">
                            <asp:TextBox ID="txtAdres" runat="server" Height="130px" Width="465px"></asp:TextBox>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Vergi Dairesi Seçiniz:</label>
                        <div class="formRight searchDrop">
                            <asp:DropDownList ID="ddlVergi"  class="chzn-select" style="width: 350px;" runat="server" OnDataBound="ddlVergi_DataBound"></asp:DropDownList>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Vergi Numarası:</label>
                        <div class="formRight">
                            <asp:TextBox ID="txtVergiNo" runat="server"></asp:TextBox>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>İş Telefonu:</label>
                        <div class="formRight">
                            <asp:TextBox ID="txtistel" runat="server"></asp:TextBox>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>Cep Telefonu:</label>
                        <div class="formRight">
                            <asp:TextBox ID="txtceptel" runat="server"></asp:TextBox>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label>E-Mail:</label>
                        <div class="formRight">
                            <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <div class="formSubmit">
                            <input type="submit" value="DÜZENLE" class="redB" />
                        </div>
                        <div class="clear"></div>
                    </div>

                </div>
            </fieldset>


        </form>

    </div>

</asp:Content>

